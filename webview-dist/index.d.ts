import { UnlistenFn } from "@tauri-apps/api/event";
export declare function sendMessage(message: string): Promise<void>;
export declare function sendObject(message: object): Promise<void>;
export declare function sendObjectWithTimestamp(message: object): Promise<void>;
export declare function subscribe(topic: string): Promise<void>;
export declare function unsubscribe(topic: string): Promise<void>;
export declare function onMessage(cb: (message: string) => void): Promise<UnlistenFn>;
