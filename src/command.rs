#[derive(Debug)]
pub enum Command {
  SendMessage { message: String },
  Subscribe { topic: String },
  Unsubscribe { topic: String },
}
