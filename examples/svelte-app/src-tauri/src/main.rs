#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]

fn main() {
  if std::env::var_os("RUST_LOG").is_none() {
    std::env::set_var("RUST_LOG", "tauri_plugin_libp2p_signal=trace");
  }

  tracing_subscriber::fmt::init();

  tauri::Builder::default()
    .plugin(tauri_plugin_libp2p_signal::init())
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}
