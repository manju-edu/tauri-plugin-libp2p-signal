# Tauri Plugin libp2p-signal

Tauri plugin for WebRTC signaling over libp2p network

## Create project

- `npm install -g @tauri-apps/cli`
- `tauri plugin init --name libp2p-signal --api`

## Development

- in root directory
  - `yarn build`
  - `cargo build`

- in example/svelte-app directory
  - `yarn build`
  - `yarn tauri dev`
  